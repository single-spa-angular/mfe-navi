import { Component } from '@angular/core';

@Component({
  selector: 'nav-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'nav';
}
